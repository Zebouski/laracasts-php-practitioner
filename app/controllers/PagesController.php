<?php

namespace App\Controllers;

use App\Core\App;

class PagesController {
    public function home() {
        $todos = App::get('database')->selectAll('todos');
        return view('index', compact('todos'));
    }
    public function about() {
        return view('about');
    }
    public function aboutCulture() {
        $company_name = 'Laracasts';
        return view('about-culture', compact('company_name'));
    }
    public function contact() {
        return view('contact');
    }
}
