<?php

return [
    'database' => [
        'name' => 'laracasts',
        'username' => 'root',
        'password' => 'password-reset',
        'connection' => 'mysql:host=127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ],
    ],
];
