<?php

namespace App\Core;

class Task {
    public $description;
    public $completed = false;

//     public function __construct($description) {
//         // automatically triggered on instantiation
//         $this->description = $description;
//     }
    public function isComplete() {
        return $this->completed;
    }
    public function complete() {
        $this->completed = true;
    }

    public function description() {

    }
}




// $tasks = [
//     new Task('Go to the store'),
//     new Task('Finish my screencast'),
//     new Task('Clean my room'),
// ];
//
// $tasks[1]->complete();
//
// dd($tasks[1]->isComplete());
// dd($tasks);
